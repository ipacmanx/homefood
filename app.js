const access = require('./access');

const Telegraf = require('telegraf');
const clients = require('./clients');
const recepts = require('./recepts');


const bot = new Telegraf(access.telegram);

bot.telegram.getMe().then((botInfo) => {
    bot.options.username = botInfo.username;
    console.log("Initialized", botInfo.username);
});

bot.start((ctx) => {
    ctx.reply(`Welcome! Thanks for message.
You can use:\n
/breakfast
/lanch
/dinner`);
    clients.add(ctx);
});

bot.command('breakfast', (ctx) => {
    console.log('breakfast');
    recepts.getRandom('breakfast').then((card) => {
        console.log(card.name);
        console.log(card);
        ctx.reply(card.name);
    })
});

bot.command('lanch', (ctx) => {
    recepts.getRandom('lanch').then((card) => {
        ctx.reply(card.name);
    })
});

bot.command('dinner', (ctx) => {
    recepts.getRandom('dinner').then((card) => {
        ctx.reply(card.name);
    })
});

bot.startPolling();