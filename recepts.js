const access = require('./access');
const Trello = require("trello");
const trello = new Trello(access.key, access.token);

trello.getCardsOnList().then((cards) => {
    cards.length
});

const recepts = {
    daytime: {
        morning: '5bc3ee4d9a3f3a14daad902a',
        day: '5bc3ef065e051c4033a0709a',
        evening: '5bc3eeed14a0843f4061834a'
    },
    type: {
        breakfast: '5bc3ee4d9a3f3a14daad902a',
        lanch: '5bc3ef065e051c4033a0709a',
        dinner: '5bc3eeed14a0843f4061834a'
    },
    getRandom: function(type){
        var self = this;
        return new Promise((resolve,reject) => {
            trello.getCardsOnList(self.type[type]).then((cards) => {
                resolve(cards[self.random(cards.length)]);
            });
        });
    },
    random: function(max, min) {
        if (min == undefined) min = 0;
        return Math.floor(Math.random() * (max - min)) + min;
    }
};

module.exports = recepts;