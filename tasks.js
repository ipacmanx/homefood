function Tasks(){
    this.store = [];
    
    this.add = function(data){
        var id = this.store.length+1;
        data.id = id;
        this.store.push(data);
        return data;
    }

    this.findById = function(){

    }

    this.run = function(){

    }

    this.remove = function(id){
        this.store = this.store.filter((task) => { if (task.id != id) return task; })
        return true;
    }
}

module.exports = Tasks;