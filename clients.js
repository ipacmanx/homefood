const Tasks = require('./tasks');
const fs = require('fs');

const clients = {
    store: [],
    add: function(data){
        var id = data.update.message.chat.id;

        this.store.push({
            id: id,
            storage: [],
            tasks: new Tasks()
        });
        this.save();
    },
    findById: function(id){
        return this.store.find((d) => d.id == id );
    },
    save: function(){
        fs.writeFileSync('db/clients.json',JSON.stringify(this.store,null,2));
    }
}

module.exports = clients;